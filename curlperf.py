#!/usr/bin/env python3
import argparse
import datetime
import subprocess
import sys

def measure_download_speed(url, timeout=None, use_ipv6=False):
    """
    :returns: The download speed for the specified URL in KB/s
    """
    #curl_output = "%{url_effective};%{time_total};%{size_download};%{speed_download}"
    curl_output = "%{speed_download}"

    if use_ipv6:
        address_family = "-6"
    else:
        address_family = "-4"

    command = [
            "curl", address_family, "-L", "--output", "/dev/null", "--silent", "--show-error", "--write-out", curl_output
    ]
    if timeout:
        command.append("--max-time")
        command.append(str(timeout))
    command.append(url)

    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, _stderr = p.communicate()
    # returncode 28 is used for timeouts
    if p.returncode not in (0, 28):
        sys.stderr.write("curl-call failed")
        sys.stderr.write(_stderr.decode("utf-8"))
        return None

    speed_download = stdout.decode("utf-8")
    bytes_speed = float(speed_download.replace(",", "."))
    return bytes_speed

def write_influxdb(database, measurement, bandwidth_value, host, port, username=None, password=None, ssl=True):
    from influxdb import InfluxDBClient

    client = InfluxDBClient(
        host=host, port=port, username=username, password=password, ssl=ssl, verify_ssl=ssl
    )
    client.switch_database(database)

    json_body = [
        {
            "measurement": measurement,
            "fields": {
                "bandwidth": int(bandwidth_value)
            }
        },
        ]
    result = client.write_points(json_body)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Measure download speed for a specified URL.')
    parser.add_argument(
        'url', nargs=1, help='The url for speed measurements. Should be a large binary file.'
    )
    parser.add_argument(
        '-t', '--timeout', type=int, help='Abort the download after this time (in seconds).'
    )
    parser.add_argument(
        '-6', '--ipv6', action='store_true', default=False,
        help="Use IPv6 instead of IPv4 for download."
    )
    parser.add_argument(
        '--csv', action='store_true', help="Output as CSV (including timestamp)"
    )
    parser.add_argument(
        '--influxdb-host', help="InfluxDB hostname"
    )
    parser.add_argument(
        '--influxdb-port', type=int, help="InfluxDB port"
    )
    parser.add_argument(
        '--influxdb-database', help="InfluxDB database name"
    )
    parser.add_argument(
        '--influxdb-measurement', help="InfluxDB Measurement"
    )
    parser.add_argument(
        '--influxdb-username', help="InfluxDB username"
    )
    parser.add_argument(
        '--influxdb-password', help="InfluxDB password"
    )

    args = parser.parse_args()

    influxdb_opts = {}
    if args.influxdb_host and args.influxdb_port and args.influxdb_database and args.influxdb_measurement:
        influxdb_opts = {
            "database": args.influxdb_database,
            "measurement": args.influxdb_measurement,
            "host": args.influxdb_host,
            "port":args.influxdb_port,
        }

    if influxdb_opts and args.influxdb_username:
        influxdb_opts["username"] = args.influxdb_username
    if influxdb_opts and args.influxdb_password:
        influxdb_opts["password"] = args.influxdb_password

    kwargs = {
        "url": args.url[0],
    }
    if args.timeout:
        kwargs["timeout"] = args.timeout
    if args.ipv6:
        kwargs["use_ipv6"] = True
    
    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    bytes_per_second = measure_download_speed(**kwargs)
    if bytes_per_second:
        bits = (bytes_per_second * 8)
        if args.csv:
            print(f"{now},{bits:.2f}")
        else:
            print(f"{bits:.2f} Bit/s")
        if influxdb_opts:
            influxdb_opts["bandwidth_value"] = bits
            write_influxdb(**influxdb_opts)
